/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef ASG_ANALYSIS_ALGORITHMS__EVENT_DECORATOR_ALG_H
#define ASG_ANALYSIS_ALGORITHMS__EVENT_DECORATOR_ALG_H

#include <xAODEventInfo/EventInfo.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <AsgTools/PropertyWrapper.h>
#include <map>
#include <functional>

namespace CP
{
  /// \brief an algorithm for decorating EventInfo

  class EventDecoratorAlg final : public EL::AnaAlgorithm
  {
  public:

    /// \brief the standard constructor
    using EL::AnaAlgorithm::AnaAlgorithm;
    StatusCode initialize () override;
    StatusCode execute () override;



  private:

    /// \brief the systematics list we run
    SysListHandle m_systematicsList {this};

    /// \brief the name of the event info object
    CP::SysReadHandle<xAOD::EventInfo> m_eventInfoHandle {this, "eventInfo", "EventInfo", "the input EventInfo object"};

    /// \brief the uint32_t decorations to add
    Gaudi::Property<std::map<std::string, uint32_t>> m_uint32Decorations {this, "uint32Decorations", {}, "the uint32_t decorations to add"};

    /// \brief the functions to add decorations
    std::vector<std::function<void(const xAOD::EventInfo&)>> m_decFunctions {};
  };
}

#endif
